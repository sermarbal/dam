﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWin : MonoBehaviour {

	void OnTriggerEnter(Collider reference){

		Destroy(reference.gameObject.GetComponent<BallController> ());

		reference.gameObject.AddComponent<Winner> ();

	}

}
