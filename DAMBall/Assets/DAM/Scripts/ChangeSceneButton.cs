﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.SceneManagement;
//
//public class ChangeSceneButton : MonoBehaviour {
//
//	// Tipo de skin de GUI que se usará para dibujar
//	public GUISkin skin;
//
//	// Rectángulo donde se dibujará el botón
//	public ScaledRect rectangle;
//
//	// Texto que mostrará el botón
//	public string text = "Button";
//
//	// Escena que se cargará al pulsarlo
//	public int scene = 0;
//
//	// Función de actualización de la GUI
//	void OnGUI ( ) {
//
//		// Indicamos que se utilice nuestra skin para dibujar
//		GUI.skin = skin;
//
//		// Dibujamos un botón de la GUI que responde a clics
//		if ( GUI.Button ( rectangle , text ) ) {
//
//			// Carga el nuevo nivel
//			SceneManager.LoadScene( scene );
//
//		}
//
//	}
//}
