﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orbite : MonoBehaviour {

	public GameObject Objective;

	// Update is called once per frame
	void Update () {

		Transform parentTransform = Objective.transform;

		transform.LookAt (parentTransform);

		transform.Translate (Vector3.right, Space.Self);

	}
}
