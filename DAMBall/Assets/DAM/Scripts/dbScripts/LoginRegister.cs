﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoginRegister : MonoBehaviour
{

	public static string user = "";
	private string password = "", rePass = "", message = "";
	
	private bool register = false;

	private void OnGUI ()
	{
		
		if (message != "")
			GUILayout.Box (message);
		
		if (register) {
			GUILayout.Label ("Username");
			user = GUILayout.TextField (user);
			GUILayout.Label ("password");
			password = GUILayout.PasswordField (password, "*" [0]);
			GUILayout.Label ("Re-password");
			rePass = GUILayout.PasswordField (rePass, "*" [0]);
			
			GUILayout.BeginHorizontal ();
			
			if (GUILayout.Button ("Back"))
				register = false;
			
			if (GUILayout.Button ("Register")) {
				message = "Registrando usuario...";
				
				if (user == "" || password == "")
					message = "Debes rellenar todos los campos \n";
				else {
					if (password == rePass) {
						WWWForm form = new WWWForm ();
						form.AddField ("user", user);
						form.AddField ("password", password);
						WWW w = new WWW ("http://labs.iam.cat/~a14sermarbal/DAM/writeBD.php", form);
						StartCoroutine (registerFunc (w));
					} else
						message = "El password no coincide!! \n";
				}
			}
			
			GUILayout.EndHorizontal ();
		} else {
			GUILayout.Label ("User:");
			user = GUILayout.TextField (user);
			GUILayout.Label ("Password:");
			password = GUILayout.PasswordField (password, "*" [0]);
			
			GUILayout.BeginHorizontal ();
			
			if (GUILayout.Button ("Login")) {
				message = "Intentando login...";
				
				if (user == "" || password == "")
					message = "Debes rellenar todos los campos \n";
				else {
					WWWForm form = new WWWForm ();
					form.AddField ("user", user);
					form.AddField ("password", password);
					WWW w = new WWW ("http://labs.iam.cat/~a14sermarbal/DAM/readDB.php", form);
					StartCoroutine (login (w));
				}
			}
			
			if (GUILayout.Button ("Register"))
				register = true;
			
			GUILayout.EndHorizontal ();
		}
	}

	IEnumerator login (WWW w)
	{
		yield return w;
		if (w.error == null) {
			SceneManager.LoadScene (0);
		} else {
			message = "ERROR: " + w.error + "\n";
		}
	}

	IEnumerator registerFunc (WWW w)
	{
		yield return w;
		if (w.error == null) {
			message = w.text;
		} else {
			message = "ERROR: " + w.error + "\n";
		}
	}
}

