﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateLightByDistancePlayer : MonoBehaviour {

	public GameObject player;
	public float maxIntensity;
	public float minIntensity;
	public float incrementIntensity;
	public float intervalIntensity;

	private float cont;

	// Update is called once per frame
	void Update () {

		float distance = Vector3.Distance(player.transform.position, transform.position);
		cont += Time.deltaTime;

		float i = GetComponent<Light> ().intensity;

		if (distance < 5 && i < maxIntensity) {
			GetComponent<Light> ().intensity += incrementIntensity;
			cont = 0;
		} else if (distance >= 5 && i > minIntensity) {
			GetComponent<Light> ().intensity -= incrementIntensity;
			cont = 0;
		}


	}
}
