﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddComponentByTrigger : MonoBehaviour {

	public GameObject target;
	public string tagElement;

	private TeleportTarget teleportTarget;
	private UpdateConstraints updateConstraints;

	void OnTriggerEnter(Collider reference){

		if (reference.gameObject.tag.Equals (tagElement)) {

			teleportTarget = target.AddComponent<TeleportTarget> ();
			teleportTarget.destinePoint="Teleport point 1";

			updateConstraints = target.AddComponent<UpdateConstraints> ();
			updateConstraints.constrait = RigidbodyConstraints.FreezePositionX;

		}

	}

	void OnTriggerExit(Collider reference){

		if (reference.gameObject.tag.Equals (tagElement)) {
			Destroy(target.gameObject.GetComponent<TeleportTarget> ());
			Destroy(target.gameObject.GetComponent<UpdateConstraints> ());
		}

	}

}
