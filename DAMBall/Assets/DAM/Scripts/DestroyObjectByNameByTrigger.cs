﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyObjectByNameByTrigger : MonoBehaviour {

	public string nameObject = "DoorTest2";

	void OnTriggerEnter(Collider reference){

		GameObject door = GameObject.Find (nameObject);

		Destroy (door);

	}

}
