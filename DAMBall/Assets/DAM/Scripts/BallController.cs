﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{

	public Camera reference;

	public float moveForce = 25;
	public float rotateForce = 25;
	public float jumpForce = 50;
	public float GroundDistance = 0.1f;

	public string HorizontalMoveController = "Horizontal";
	public string VerticalMoveController = "Vertical";
	public string JumpController = "Jump";

	private bool buttonsShowed = false;

	// Use this for initialization
	void Start ()
	{

	}

	void Update ()
	{

		if (!buttonsShowed)
		if (transform.position.y < -200) {
//			reference.gameObject.AddComponent<Loser> ();
			buttonsShowed = true;
		}

	}

	// Update is called once per frame
	void FixedUpdate ()
	{

		// Current rb object
		Rigidbody rb = GetComponent<Rigidbody> ();

		float input;
		float axis;
		Vector3 result;

		// Local ship movement

		// up down Z
		input = Input.GetAxis (HorizontalMoveController);
		axis = input * moveForce * Time.deltaTime;

		result = reference.transform.right * moveForce * axis;
		result.y = 0f;

		rb.AddForce (result, ForceMode.Acceleration);



		// up down Y
		input = Input.GetAxis (VerticalMoveController);
		axis = input * moveForce * Time.deltaTime;

		result = reference.transform.forward * moveForce * axis;
		result.y = 0f;

		rb.AddForce (result, ForceMode.Acceleration);

		if (IsGrounded ()) {
			input = Input.GetAxis (JumpController);

			axis = input * jumpForce * Time.deltaTime;
			rb.AddForce (Vector3.up * jumpForce * axis, ForceMode.Acceleration);
		}

	}

	bool IsGrounded ()
	{
		return Physics.Raycast (transform.position, -Vector3.up, GroundDistance + 0.1f);
	}

}
