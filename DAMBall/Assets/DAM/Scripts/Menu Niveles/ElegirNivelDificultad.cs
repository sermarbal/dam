﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ElegirNivelDificultad : MonoBehaviour {

    public RawImage rawImgFondo;
    public Texture texturaFondoFacil;
    public Texture texturaFondoNormal;
    public Texture texturaFondoDificil;

    public Canvas menuPrincipal;
    public Canvas menuDificultad;
    public Canvas menuModos;
        
    public Button facil;
    public Button normal;
    public Button dificil;
    public Button volver;

    private static string modoselected;
    // Use this for initialization
    void Start()
    {
        //Canvas
        menuPrincipal = menuPrincipal.GetComponent<Canvas>();
        menuDificultad = menuDificultad.GetComponent<Canvas>();
        menuModos = menuModos.GetComponent<Canvas>();

        //Buttons
        facil = facil.GetComponent<Button>();
        normal = normal.GetComponent<Button>();
        dificil = dificil.GetComponent<Button>();
        volver = volver.GetComponent<Button>();
    }

    internal static string GetMode()
    {
        return modoselected;
    }

    // Update is called once per frame
    void Update()
    {

        //OnClick
        facil.onClick.AddListener(() => modosFacil());
        normal.onClick.AddListener(() => modosNormal());
        dificil.onClick.AddListener(() => modosDificil());
        volver.onClick.AddListener(() => volverPress());

    }

    private void volverPress()
    {
        menuDificultad.enabled = false;
        menuPrincipal.enabled = true;
    }

    private void modosDificil()
    {
        modoselected = "dificil";
        menuModos.enabled = true;
        menuDificultad.enabled = false;
        rawImgFondo.texture = texturaFondoDificil;
    }

    private void modosNormal()
    {
        modoselected = "normal";
        menuModos.enabled = true;
        menuDificultad.enabled = false;
        rawImgFondo.texture = texturaFondoNormal;
    }

    private void modosFacil()
    {
        modoselected = "facil";
        menuModos.enabled = true;
        menuDificultad.enabled = false;
        rawImgFondo.texture = texturaFondoFacil;

    }
}
