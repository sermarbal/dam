﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ModoJuego : MonoBehaviour {

    //Fondo
    public RawImage rawImgFondo;

    public Texture texturaFondoFacil;
    public Texture texturaFondoEstandard;

    //Canvas
    public Canvas menuJuego;
    public Canvas menuModo;
    public Canvas menuListaNivelesLibre;

    //Buttons
    public Button bttHistoria;
    public Button bttContra;
    public Button bttLibre;
    public Button volver;

    

	// Use this for initialization
	void Start () {

        //Fondo
        rawImgFondo = rawImgFondo.GetComponent<RawImage>();

        //Canvas
        menuJuego = menuJuego.GetComponent<Canvas>();
        menuModo = menuModo.GetComponent<Canvas>();
        menuListaNivelesLibre = menuListaNivelesLibre.GetComponent<Canvas>();

        //Button
        bttHistoria = bttHistoria.GetComponent<Button>();
        bttContra = bttContra.GetComponent<Button>();
        bttLibre = bttLibre.GetComponent<Button>();
        volver = volver.GetComponent<Button>();

	}
	
	// Update is called once per frame
	void Update () {

        bttHistoria.onClick.AddListener(() => historiaPress());
        bttContra.onClick.AddListener(() => contraPress());
        bttLibre.onClick.AddListener(() => librePress());
        volver.onClick.AddListener(() => volverPress());

	}

    private void volverPress()
    {
        rawImgFondo.texture = texturaFondoEstandard;
        menuJuego.enabled = true;
        menuModo.enabled = false;
    }

    private void librePress()
    {
        menuListaNivelesLibre.enabled = true;
        menuModo.enabled = false;
    }

    private void contraPress()
    {
        throw new NotImplementedException();
    }

    private void historiaPress()
    {
        throw new NotImplementedException();
    }
}
