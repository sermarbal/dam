﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Libre : MonoBehaviour
{

	//Canvas
	public Canvas menuListaNiveles;
	public Canvas menuModos;

	//Button
	public Button nivelUno;
	public Button nivelDos;
	public Button nivelTres;
	public Button nivelCuatro;
	public Button nivelCinco;
	public Button nivelSeis;
	public Button nivelSiete;
	public Button nivelOcho;
	public Button nivelNueve;

	public Button volver;
	private string modoActual = "normal";

	// Use this for initialization
	void Start ()
	{

		//Canvas
		menuListaNiveles = menuListaNiveles.GetComponent<Canvas> ();
		menuModos = menuModos.GetComponent<Canvas> ();

		//Button
		nivelUno = nivelUno.GetComponent<Button> ();
		nivelDos = nivelDos.GetComponent<Button> ();
		nivelTres = nivelTres.GetComponent<Button> ();
		nivelCuatro = nivelCuatro.GetComponent<Button> ();
		nivelCinco = nivelCinco.GetComponent<Button> ();
		nivelSeis = nivelSeis.GetComponent<Button> ();
		nivelSiete = nivelSiete.GetComponent<Button> ();
		nivelOcho = nivelOcho.GetComponent<Button> ();
		nivelNueve = nivelNueve.GetComponent<Button> ();

		volver = volver.GetComponent<Button> ();
	}
	
	// Update is called once per frame
	void Update ()
	{

		nivelUno.onClick.AddListener (() => selectLvl ("nivelUno"));
		nivelDos.onClick.AddListener (() => selectLvl ("nivelDos"));
		nivelTres.onClick.AddListener (() => selectLvl ("nivelTres"));
		nivelCuatro.onClick.AddListener (() => selectLvl ("nivelCuatro"));
		nivelCinco.onClick.AddListener (() => selectLvl ("nivelCinco"));
		nivelSeis.onClick.AddListener (() => selectLvl ("nivelSeis"));
		nivelSiete.onClick.AddListener (() => selectLvl ("nivelSiete"));
		nivelOcho.onClick.AddListener (() => selectLvl ("nivelOcho"));
		nivelNueve.onClick.AddListener (() => selectLvl ("nivelNueve"));

		volver.onClick.AddListener (() => volverPress ());
	}

	private void volverPress ()
	{
		menuListaNiveles.enabled = false;
		menuModos.enabled = true;
	}

	private void selectLvl (String nivel)
	{
		
		//FACIL
		if (modoActual.Equals ("facil")) {
			if (nivel.Equals ("nivelUno")) {
				SceneManager.LoadScene (1);
			} else if (nivel.Equals ("nivelDos")) {
				SceneManager.LoadScene (2);
			} else if (nivel.Equals ("nivelTres")) {
				SceneManager.LoadScene (3);
			} else if (nivel.Equals ("nivelCuatro")) {
				SceneManager.LoadScene (4);
			} else if (nivel.Equals ("nivelCinco")) {
				SceneManager.LoadScene (5);
			} else if (nivel.Equals ("nivelSeis")) {
				SceneManager.LoadScene (6);
			} else if (nivel.Equals ("nivelSiete")) {
				SceneManager.LoadScene (7);
			} else if (nivel.Equals ("nivelOcho")) {
				SceneManager.LoadScene (8);
			} else if (nivel.Equals ("nivelNueve")) {
				SceneManager.LoadScene (9);
			}


			//NORMAL
		} else if (modoActual.Equals ("normal")) {
			
			if (nivel.Equals ("nivelUno")) { 
				SceneManager.LoadScene (1);
//				menuListaNiveles.enabled = false;
//				menuModos.enabled = true;
			} else if (nivel.Equals ("nivelDos")) {
				SceneManager.LoadScene (2);
			} else if (nivel.Equals ("nivelTres")) {
				SceneManager.LoadScene (3);
			} else if (nivel.Equals ("nivelCuatro")) {
				SceneManager.LoadScene (4);
			} else if (nivel.Equals ("nivelCinco")) {
				SceneManager.LoadScene (5);
			} else if (nivel.Equals ("nivelSeis")) {
				SceneManager.LoadScene (6);
			} else if (nivel.Equals ("nivelSiete")) {
				SceneManager.LoadScene (7);
			} else if (nivel.Equals ("nivelOcho")) {
				SceneManager.LoadScene (8);
			} else if (nivel.Equals ("nivelNueve")) {
				SceneManager.LoadScene (9);
			}


			//DIFICIL
		} else if (modoActual.Equals ("dificil")) {
			if (nivel.Equals ("nivelUno")) {
				SceneManager.LoadScene (1);
			} else if (nivel.Equals ("nivelDos")) {
				SceneManager.LoadScene (2);
			} else if (nivel.Equals ("nivelTres")) {
				SceneManager.LoadScene (3);
			} else if (nivel.Equals ("nivelCuatro")) {
				SceneManager.LoadScene (4);
			} else if (nivel.Equals ("nivelCinco")) {
				SceneManager.LoadScene (5);
			} else if (nivel.Equals ("nivelSeis")) {
				SceneManager.LoadScene (6);
			} else if (nivel.Equals ("nivelSiete")) {
				SceneManager.LoadScene (7);
			} else if (nivel.Equals ("nivelOcho")) {
				SceneManager.LoadScene (8);
			} else if (nivel.Equals ("nivelNueve")) {
				SceneManager.LoadScene (9);
			}
		}
	}
}
