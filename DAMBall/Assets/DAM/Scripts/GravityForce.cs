﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityForce : MonoBehaviour
{

	public float force;

	void OnTriggerEnter(Collider reference){

		reference.gameObject.GetComponent<Collider> ().material = null;

	}

	void OnTriggerStay (Collider reference)
	{
		if (reference.name.Equals ("PlayerBall")) {
			Rigidbody rb = reference.gameObject.GetComponent<Rigidbody> ();
			rb.AddForce (Vector3.up * Time.deltaTime * force, ForceMode.Acceleration);
		}
	}



}
