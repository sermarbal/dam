﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyParentByTrigger : MonoBehaviour
{

	void OnTriggerEnter (Collider reference)
	{
		Destroy (transform.parent.gameObject);
	}

	void OnTriggerStay (Collider reference)
	{

	}

	void OnTriggerExit (Collider referencia)
	{
		Destroy (transform.gameObject);
	}

}
