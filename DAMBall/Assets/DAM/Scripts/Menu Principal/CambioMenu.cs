﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CambioMenu : MonoBehaviour {

    public new AudioSource audio;

    //Canvas
    public Canvas menuPrincipal; //MENU PRINCIPAL

    public Canvas menuJugar; //JUEGOS (Accesible desde menuPrincipal)
    public Canvas menuModos; //MODOS (Accesible desde menuJugar)
    public Canvas menuListaNivelesLibre; //NIVELES (Accesible desde menuModos)

    public Canvas menuOpciones; //OPCIONES (Accesible desde menuPrincipal)

    public Canvas menuRecord; //RECORD (Accesible desde menuPrincipal)

    public Canvas menuSalir; //SALIR  (Accesible desde menuPrincipal)

    //Botones
    public Button bJugar;
    public Button bOpciones;
    public Button bRecord;
    public Button bExit;

    public Button bMute;

    void Start()
    {
        //Canvas 
        menuPrincipal = menuPrincipal.GetComponent<Canvas>();
        menuJugar = menuJugar.GetComponent<Canvas>();
        menuModos = menuModos.GetComponent<Canvas>();
        menuListaNivelesLibre = menuListaNivelesLibre.GetComponent<Canvas>();
        menuOpciones = menuOpciones.GetComponent<Canvas>();
        menuRecord = menuRecord.GetComponent<Canvas>();
        menuSalir = menuSalir.GetComponent<Canvas>();

        //Disable Canvas not Used at Start
        menuJugar.enabled = false;
        menuModos.enabled = false;
        menuListaNivelesLibre.enabled = false;
        menuOpciones.enabled = false;
        menuRecord.enabled = false;
        menuSalir.enabled = false;

        //Buttons
        bJugar = bJugar.GetComponent<Button>();
        bOpciones = bOpciones.GetComponent<Button>();
        bRecord = bRecord.GetComponent<Button>();
        bExit = bExit.GetComponent<Button>();

        //Music
        bMute = bMute.GetComponent<Button>();
    }

    void Update()
    {
        //ONCLICK
        bJugar.onClick.AddListener(() => JugarPress());
        bOpciones.onClick.AddListener(() => OptionPress());
        bRecord.onClick.AddListener(() => RecordPress());
        bExit.onClick.AddListener(() => ExitPress());
        bMute.onClick.AddListener(() => muteSound()); 
    }

   

    //Methods Pressed Buttons
    public void JugarPress()
    {
        menuJugar.enabled = true;
        menuPrincipal.enabled = false;
    }

    private void OptionPress()
    {
        menuPrincipal.enabled = false;
        menuOpciones.enabled = true;
       
    }

    private void RecordPress()
    {
        menuPrincipal.enabled = false;
        menuRecord.enabled = true;
    }
    private void ExitPress()
    {
        menuPrincipal.enabled = false;
        menuSalir.enabled = true;
    }

    //Music Mute/Demute
    public void muteSound()
    {

        audio = audio.GetComponent<AudioSource>();

        if(audio.mute)
        {
            audio.mute = false;
        }else
        {
            audio.mute = true;
        }
       
         
    }
}
