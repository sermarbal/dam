﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportTarget : MonoBehaviour {

	public string destinePoint;

	void OnTriggerEnter(Collider reference){

		print ("teleport!");
		if (reference.gameObject.name.Equals ("PlayerBall")) {
			GameObject tp1 = GameObject.Find(destinePoint);
			reference.transform.position = tp1.transform.position;
		}

	}

}
