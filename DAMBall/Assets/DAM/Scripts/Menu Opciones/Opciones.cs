﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Opciones : MonoBehaviour {

    public Canvas menuPrincipal;
    public Canvas menuOpciones;

    public Slider slider;
    public new AudioSource audio;
    public Button vuelta;

    private float posicion;



    // Use this for initialization
    void Start () {

        //Canvas
        menuPrincipal = menuPrincipal.GetComponent<Canvas>();
        menuOpciones = menuOpciones.GetComponent<Canvas>();

        //Button
        vuelta = vuelta.GetComponent<Button>();

        //Slider
        slider = slider.GetComponent<Slider>();

        //Audio
        audio = audio.GetComponent<AudioSource>();

	}
	
	// Update is called once per frame
	void Update () {

        vuelta.onClick.AddListener(() => volver());
        audio.volume = slider.value;

    }

    private void volver()
    {
        menuPrincipal.enabled = true;
        menuOpciones.enabled = false;
    }
}
