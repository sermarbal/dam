﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ExitOption : MonoBehaviour {

    public Button Yes;
    public Button No;

    public Canvas menuPrincipal;
    public Canvas menuExit;

    // Use this for initialization
    void Start () {

        //Canvas
        menuPrincipal.GetComponent<Canvas>();
        menuExit.GetComponent<Canvas>();

        //Buttons
        Yes = Yes.GetComponent<Button>();
        No = No.GetComponent<Button>();
	}
	
	// Update is called once per frame
	void Update () {

        //Onclick
        Yes.onClick.AddListener(() => YesPressed());
        No.onClick.AddListener(() => NoPressed());
	}

    private void NoPressed()
    {
        menuPrincipal.enabled = true;
        menuExit.enabled = false;
    }

    private void YesPressed()
    {
        Application.Quit(); //CLOSE APPLICATION 
    }
}
