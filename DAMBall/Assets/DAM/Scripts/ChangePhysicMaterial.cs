﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangePhysicMaterial : MonoBehaviour
{

	public PhysicMaterial physicMaterial;
	// public PhysicMaterial defaultMaterial = null;

	void OnCollisionEnter (Collision reference)
	{
		// defaultMaterial = reference.gameObject.GetComponent<Collider> ().material;
		Collider capsule = reference.gameObject.GetComponent<Collider> ();
		capsule.material = physicMaterial;
	}

//		void OnCollisionExit(Collision reference){
//	
//			print ("Exit!");
//			Collider capsule = reference.gameObject.GetComponent<Collider>();
//			capsule.material = defaultMaterial;
//	
//		}

}
