﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BallController3 : MonoBehaviour
{
	// Texto count.
	public Text countText;
	// Texto Win.
	public Text winText;
	// Texto Tiempo.
	public Text timer;

	private float maxTime;
	private float timePassed;
	private float currentTime;


	// Contador de cuadrados.
	private int count;
	private int maxCount;

	void Start ()
	{
		count = 0;
		maxCount = 8;
		SetCountText ();
		winText.text = "";
		maxTime = 180;
		timePassed = 0;
		currentTime = 0;
	}

	void Update(){
		currentTime = ((int)maxTime-(int)(timePassed+=Time.deltaTime));
		timer.text = currentTime + "";
	}

	void OnTriggerEnter (Collider other)
	{
		if (other.gameObject.CompareTag ("Pick Up")) {
			other.gameObject.SetActive (false);
			count = count + 1;
			SetCountText ();
		}
	}

	void SetCountText ()
	{
		countText.text = "Stars: " + count+"/"+maxCount;
	}
}